<?php
/*
 * SaaSpot Theme's Functions
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/**
 * Define - Folder Paths
 */
define( 'SAASPOT_THEMEROOT_PATH', get_template_directory() );
define( 'SAASPOT_THEMEROOT_URI', get_template_directory_uri() );
define( 'SAASPOT_CSS', SAASPOT_THEMEROOT_URI . '/assets/css' );
define( 'SAASPOT_IMAGES', SAASPOT_THEMEROOT_URI . '/assets/images' );
define( 'SAASPOT_SCRIPTS', SAASPOT_THEMEROOT_URI . '/assets/js' );
define( 'SAASPOT_FRAMEWORK', get_template_directory() . '/inc' );
define( 'SAASPOT_LAYOUT', get_template_directory() . '/layouts' );
define( 'SAASPOT_CS_IMAGES', SAASPOT_THEMEROOT_URI . '/inc/theme-options/theme-extend/images' );
define( 'SAASPOT_CS_FRAMEWORK', get_template_directory() . '/inc/theme-options/theme-extend' ); // Called in Icons field *.json
define( 'SAASPOT_ADMIN_PATH', get_template_directory() . '/inc/theme-options/cs-framework' ); // Called in Icons field *.json

/**
 * Define - Global Theme Info's
 */
if (is_child_theme()) { // If Child Theme Active
	$saaspot_theme_child = wp_get_theme();
	$saaspot_get_parent = $saaspot_theme_child->Template;
	$saaspot_theme = wp_get_theme($saaspot_get_parent);
} else { // Parent Theme Active
	$saaspot_theme = wp_get_theme();
}
define('SAASPOT_NAME', $saaspot_theme->get( 'Name' ));
define('SAASPOT_VERSION', $saaspot_theme->get( 'Version' ));
define('SAASPOT_BRAND_URL', $saaspot_theme->get( 'AuthorURI' ));
define('SAASPOT_BRAND_NAME', $saaspot_theme->get( 'Author' ));

/**
 * All Main Files Include
 */
require_once( SAASPOT_FRAMEWORK . '/init.php' );
