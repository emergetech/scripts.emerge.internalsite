<script>


/**************************************
 Get Parameters from URL
***************************************/
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


var params;
var selectedPlan = null;

params = getUrlVars();
selectedPlan = params['selectedPlan'];

if(selectedPlan) {
  selectedPlan = decodeURI(selectedPlan);

  //alert('selected plan - '+selectedPlan);

  //Setting up Quote screen for resulting plan
  if(selectedPlan == 'Standard Plan') {
    document.getElementById('selected-plan').innerText = selectedPlan;
    document.getElementById('count1').innerHTML = 21;
    document.getElementById('count2').innerHTML = 27;
  } else if(selectedPlan == 'Professional Plan') {
    document.getElementById('selected-plan').innerText = selectedPlan;
    document.getElementById('count1').innerHTML = 40;
    document.getElementById('count2').innerHTML = 46;
  } else if(selectedPlan == 'Enterprise Plan') {
    document.getElementById('selected-plan').innerText = selectedPlan;
    document.getElementById('count1').innerHTML = 67;
    document.getElementById('count2').innerHTML = 79;
  }

  /*************************************
    Calculate animation
  ************************************/

  $('#count1').each(function () {
      $(this).prop('Counter',0).animate({
          Counter: $(this).text()
      }, {
          duration: 2500,
          easing: 'swing',
          step: function (now) {
              $(this).text(Math.ceil(now));
          }
      });
  });

  $('#count2').each(function () {
      $(this).prop('Counter',0).animate({
          Counter: $(this).text()
      }, {
          duration: 2500,
          easing: 'swing',
          step: function (now) {
              $(this).text(Math.ceil(now));
          }
      });
  });

}

/**************************************
 Checks if Standard Plan is a fit
***************************************/

function checkIfStandardPlan(valuesObj) {

  var checkSupport = false;
  var checkConnection = false;

  //checks if any of the checkboxes are checked, standard plan is not a good fit

  if(valuesObj.salesforceLightning) {
    return false;
  } else if(valuesObj.needMoreLocalAgents) {
    return false;
  } else if(valuesObj.needEmergencySupport) {
    return false;
  } else if(valuesObj.enterpriseConnection) {
    return false;
  } else if(valuesObj.moreEnvironments) {
    return false;
  }else if(valuesObj.clusterAgents) {
    return false;
  }

  if(valuesObj.numOfConnections == '2' || valuesObj.numOfConnections == '3') {
    checkConnection = true;
  }
  /*
  if(valuesObj.lvlOfSupport == 'low-support') {
    checkSupport = true;
  }
*/
  if(checkConnection == true /*&& checkSupport == true*/) {
    return true;
  } else {
    return false;
  }

}

/**************************************
Checks if Professional Plan is a fit
***************************************/

function checkIfProfessionalPlan(valuesObj) {

  //alert('values - '+JSON.stringify(valuesObj));
  /*
  if(valuesObj.lvlOfSupport == 'med-support') {
    return true;
  }*/

  if(valuesObj.enterpriseConnection) {
    return false;
  }

  if(valuesObj.numOfConnections == '5') {
    return true;
  }

  if(valuesObj.numOfConnections != '8' && valuesObj.allApiAddOns == true) {
    return true;
  }

  if(valuesObj.moreEnvironments == true && valuesObj.numOfConnections != '8') {
    return true;
  }

  if(valuesObj.salesforceLightning == true && valuesObj.numOfConnections != '8') {
    return true;
  }

  if(valuesObj.numOfConnections == '5' && valuesObj.moreEnvironments == true && valuesObj.allApiAddOns == true/* && valuesObj.lvlOfSupport == 'med-support'*/) {
    return true;
  } else {
    return false;
  }


}

/**************************************
Checks if Enterprise Plan is a fit
***************************************/

function checkIfEnterprisePlan(valuesObj) {

    /*if(valuesObj.lvlOfSupport == 'high-support') {
      return true;
    }

    if(valuesObj.needEmergencySupport) {
      return true;
    }*/

    if(valuesObj.enterpriseConnection) {
      return true;
    }

    if(valuesObj.clusterAgents && valuesObj.numOfConnections != '8') {
      return true;
    }

    if(valuesObj.numOfConnections == '8') {
      return true;
    }

    if(valuesObj.needMoreLocalAgents == true) {
      return true;
    }

}

/**************************************
 Show price quote and Jitterbit plan
 div and displays the Lead Form below
 the quote
***************************************/
function showJitterbitQuote() {
    try {

    // Declare all variables
   var numOfConnectionField, lvlOfSupportField;
    var isStandardPlan = false;
    var isProfessionalPlan = false;
    var isEnterprisePlan = false;
    var selectedPlan = '';
    var valuesObj = {};


    numOfConnectionField = document.getElementById("input_26_9");
    valuesObj.numOfConnections = numOfConnectionField.options[numOfConnectionField.selectedIndex].value;

    valuesObj.salesforceLightning = document.querySelector("input[value='salesforce-lightning']").checked;
    valuesObj.enterpriseConnection = document.querySelector("input[value='enterprise-connection']").checked;
    valuesObj.needMoreLocalAgents = document.querySelector("input[value='local-agents']").checked;
    valuesObj.moreEnvironments = document.querySelector("input[value='environments']").checked;
    //valuesObj.needEmergencySupport = document.getElementById("emergency-support").checked;
    valuesObj.clusterAgents = document.querySelector("input[value='cluster-agents']").checked;
    valuesObj.allApiAddOns = document.querySelector("input[value='api-addons']").checked;

    //Checks if Standard Plan is a fit
    isStandardPlan = checkIfStandardPlan(valuesObj);

    //Checks if Professional Plan is a fit
    isProfessionalPlan = checkIfProfessionalPlan(valuesObj);

    //Checks if Enterprise Plan is a fit
    isEnterprisePlan = checkIfEnterprisePlan(valuesObj);

    if(isStandardPlan) {
      selectedPlan = 'Standard Plan';
    }

    if(isProfessionalPlan) {
      selectedPlan = 'Professional Plan';
    }

    if(isEnterprisePlan) {
      selectedPlan = 'Enterprise Plan';
    }

    //alert('selected '+selectedPlan);

    //send email
    var priceRange = '';
    if(selectedPlan == 'Standard Plan') {
      priceRange = '21k - 27k';
    } else if(selectedPlan == 'Professional Plan') {
      priceRange = '40k - 46k';
    } else if(selectedPlan == 'Enterprise Plan') {
      priceRange = '67k - 79k';
    }


    //set selected plan to hidden field on Gravity form
    document.getElementById('input_26_11').value = selectedPlan;
    document.getElementById('input_26_12').value = priceRange;

      //submits gravity form
      document.getElementById("gform_26").submit();



  } catch(e) {
    console.log(e.message);
  }
}



</script>
