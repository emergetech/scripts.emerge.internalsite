
/**************************************
 Get Parameters from URL
***************************************/
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


var params;
var selectedPlan = null;

params = getUrlVars();
selectedPlan = params['selectedPlan'];

if(selectedPlan) {
  selectedPlan = decodeURI(selectedPlan);

  if(selectedPlan.includes('+')) {
    selectedPlan = selectedPlan.replace('+', ' ');
  }

  //alert('selected plan - '+selectedPlan);

  //Setting up Quote screen for resulting plan
  if(selectedPlan == 'Standard Plan') {
    document.getElementById('selected-plan').innerText = selectedPlan;
    document.getElementById('count1').innerHTML = 21;
    document.getElementById('count2').innerHTML = 27;
  } else if(selectedPlan == 'Professional Plan') {
    document.getElementById('selected-plan').innerText = selectedPlan;
    document.getElementById('count1').innerHTML = 40;
    document.getElementById('count2').innerHTML = 46;
  } else if(selectedPlan == 'Enterprise Plan') {
    document.getElementById('selected-plan').innerText = selectedPlan;
    document.getElementById('count1').innerHTML = 67;
    document.getElementById('count2').innerHTML = 79;
  }

  /*************************************
    Calculate animation
  ************************************/

  $('#count1').each(function () {
      $(this).prop('Counter',0).animate({
          Counter: $(this).text()
      }, {
          duration: 2500,
          easing: 'swing',
          step: function (now) {
              $(this).text(Math.ceil(now));
          }
      });
  });

  $('#count2').each(function () {
      $(this).prop('Counter',0).animate({
          Counter: $(this).text()
      }, {
          duration: 2500,
          easing: 'swing',
          step: function (now) {
              $(this).text(Math.ceil(now));
          }
      });
  });

}

/**************************************
 Checks if Standard Plan is a fit
***************************************/

function checkIfStandardPlan(valuesObj) {

  var checkSupport = false;
  var checkConnection = false;

  //checks if any of the checkboxes are checked, standard plan is not a good fit

  if(valuesObj.salesforceLightning) {
    return false;
  } else if(valuesObj.needMoreLocalAgents) {
    return false;
  } else if(valuesObj.needEmergencySupport) {
    return false;
  } else if(valuesObj.enterpriseConnection) {
    return false;
  } else if(valuesObj.moreEnvironments) {
    return false;
  }else if(valuesObj.clusterAgents) {
    return false;
  }

  if(valuesObj.numOfConnections == '2' || valuesObj.numOfConnections == '3') {
    checkConnection = true;
  }
  /*
  if(valuesObj.lvlOfSupport == 'low-support') {
    checkSupport = true;
  }
*/
  if(checkConnection == true /*&& checkSupport == true*/) {
    return true;
  } else {
    return false;
  }

}

/**************************************
Checks if Professional Plan is a fit
***************************************/

function checkIfProfessionalPlan(valuesObj) {

  //alert('values - '+JSON.stringify(valuesObj));
  /*
  if(valuesObj.lvlOfSupport == 'med-support') {
    return true;
  }*/

  if(valuesObj.enterpriseConnection) {
    return false;
  }

  if(valuesObj.numOfConnections == '5') {
    return true;
  }

  if(valuesObj.numOfConnections != '8' && valuesObj.allApiAddOns == true) {
    return true;
  }

  if(valuesObj.moreEnvironments == true && valuesObj.numOfConnections != '8') {
    return true;
  }

  if(valuesObj.salesforceLightning == true && valuesObj.numOfConnections != '8') {
    return true;
  }

  if(valuesObj.numOfConnections == '5' && valuesObj.moreEnvironments == true && valuesObj.allApiAddOns == true/* && valuesObj.lvlOfSupport == 'med-support'*/) {
    return true;
  } else {
    return false;
  }


}

/**************************************
Checks if Enterprise Plan is a fit
***************************************/

function checkIfEnterprisePlan(valuesObj) {

    /*if(valuesObj.lvlOfSupport == 'high-support') {
      return true;
    }

    if(valuesObj.needEmergencySupport) {
      return true;
    }*/

    if(valuesObj.enterpriseConnection) {
      return true;
    }

    if(valuesObj.clusterAgents && valuesObj.numOfConnections != '8') {
      return true;
    }

    if(valuesObj.numOfConnections == '8') {
      return true;
    }

    if(valuesObj.needMoreLocalAgents == true) {
      return true;
    }

}

/**************************************
 Show price quote and Jitterbit plan
 div and displays the Lead Form below
 the quote
***************************************/
function showJitterbitQuote() {
    try {

    // Declare all variables
    var numOfConnectionField, lvlOfSupportField;
    var isStandardPlan = false;
    var isProfessionalPlan = false;
    var isEnterprisePlan = false;
    var selectedPlan = '';
    var valuesObj = {};
    var nameField = document.getElementById('name-field').value;
    var emailField = document.getElementById('email-field').value;
    var phoneField = document.getElementById('phone-field').value;
    var companyField = document.getElementById('company-field').value;


    //alert(nameField);
    //alert(emailField);

   if(emailField.length > 0) {
      var isEmailValid = validateEmail(emailField);

      if(!isEmailValid) {
        return;
      }

    }

    if(phoneField.length > 0) {
      var isPhoneValid = validatePhoneNumber(phoneField);

      if(!isPhoneValid) {
        return;
      }

    }

    if(companyField.length > 0) {

      var companyRegex = new RegExp(/^[a-zA-Z 0-9]*$/);
      var isCompanyInputValid = companyRegex.test(companyField);

      if(!isCompanyInputValid) {
        alert('Please type Company Name using only alphanumeric characters.');
        return;
      }

    }

    if(nameField.length > 0) {
      var nameRegex = new RegExp(/^[a-zA-Z ]*$/);
      var isNameValid = nameRegex.test(nameField);

      if(!isNameValid) {
        alert('Please type in your full name. No special characters or numbers.');
        return;
      }

    }

    if(nameField == '' && emailField == '') {
      alert('Please fill in the following fields: Name, Email');
      return;
    }

    if(nameField == '' && emailField != '') {
      alert('Please fill in the following fields: Name');
      return;
    }

    if(nameField != '' && emailField == '') {
      alert('Please fill in the following fields: Email');
      return;
    }

    numOfConnectionField = document.getElementById("num-of-connections")
    valuesObj.numOfConnections = numOfConnectionField.options[numOfConnectionField.selectedIndex].value;

    /*lvlOfSupportField = document.getElementById("lvl-of-support");
    valuesObj.lvlOfSupport = lvlOfSupportField.options[lvlOfSupportField.selectedIndex].value;*/

    valuesObj.salesforceLightning = document.getElementById("salesforce-lightning").checked;
    valuesObj.enterpriseConnection = document.getElementById("enterprise-connection").checked;
    valuesObj.needMoreLocalAgents = document.getElementById("local-agents").checked;
    valuesObj.moreEnvironments = document.getElementById("environments").checked;
    //valuesObj.needEmergencySupport = document.getElementById("emergency-support").checked;
    valuesObj.clusterAgents = document.getElementById("cluster-agents").checked;
    valuesObj.allApiAddOns = document.getElementById("api-addons").checked;

    //Checks if Standard Plan is a fit
    isStandardPlan = checkIfStandardPlan(valuesObj);

    //Checks if Professional Plan is a fit
    isProfessionalPlan = checkIfProfessionalPlan(valuesObj);

    //Checks if Enterprise Plan is a fit
    isEnterprisePlan = checkIfEnterprisePlan(valuesObj);

    if(isStandardPlan) {
      selectedPlan = 'Standard Plan';
    }

    if(isProfessionalPlan) {
      selectedPlan = 'Professional Plan';
    }

    if(isEnterprisePlan) {
      selectedPlan = 'Enterprise Plan';
    }

    //alert('selected '+selectedPlan);

    //send email
    var priceRange = '';
    if(selectedPlan == 'Standard Plan') {
      priceRange = '21k - 27k';
    } else if(selectedPlan == 'Professional Plan') {
      priceRange = '40k - 46k';
    } else if(selectedPlan == 'Enterprise Plan') {
      priceRange = '67k - 79k';
    }

    var postUrl = './quote.php?selectedPlan='+selectedPlan;
    var form = $("<form method='POST' action='"+postUrl+"'></form>");
  	 form.append('<input type="text" name="name" value="'+nameField+'"/>');
     form.append('<input type="text" name="email" value="'+emailField+'"/>');
     form.append('<input type="text" name="main-phone" value="'+phoneField+'"/>');
     form.append('<input type="text" name="company" value="'+companyField+'"/>');
     form.append('<input type="text" name="connections" value="'+valuesObj.numOfConnections+'"/>');

     form.append('<input type="text" name="salesforce-connection" value="'+valuesObj.salesforceLightning+'"/>');
     form.append('<input type="text" name="enterprise-connection" value="'+valuesObj.enterpriseConnection+'"/>');
     form.append('<input type="text" name="more-agents" value="'+valuesObj.needMoreLocalAgents+'"/>');
     form.append('<input type="text" name="more-environments" value="'+valuesObj.moreEnvironments+'"/>');
     form.append('<input type="text" name="cluster-agents" value="'+valuesObj.clusterAgents+'"/>');
     form.append('<input type="text" name="all-api-addons" value="'+valuesObj.allApiAddOns+'"/>');

     form.append('<input type="text" name="price-range" value="'+priceRange+'"/>');


  		form.append('<input type="submit">');
  		$('body').append(form);
      form.submit();


  } catch(e) {
    console.log(e.message);
  }
}


///Validates email field
function validateEmail(mail)
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return true;
  }
    alert("You have entered an invalid email address!")
    return false;
}

//validates phone field
function validatePhoneNumber(phoneNum) {

  var pattern = new RegExp(/[0-9]{3}-[0-9]{3}-[0-9]{4}|1-[0-9]{3}-[0-9]{3}-[0-9]{4}|[0-9]{11}|[0-9]{10}$/g);

  if(pattern.test(phoneNum)) {
    return true;
  }

  alert("You have entered an invalid phone number!");
  return false;

}

/**********************************
Directs user back to quote form
***********************************/

function backToQuoteForm() {

  window.location.assign('./index.php');

}
