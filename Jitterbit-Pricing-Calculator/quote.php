<?php
// the message
$msg = "";

$msg .= "Name: ".$_POST['name'];
$msg .= "\nEmail: ".$_POST['email'];
$msg .= "\nPhone: ".$_POST['main-phone'];
$msg .= "\nCompany: ".$_POST['company'];
$msg .= "\nNumber of Connections: ".$_POST['connections'];

$msg .= "\nSalesforce Connection: ".$_POST['salesforce-connection'];
$msg .= "\nEnterprise Connection: ".$_POST['enterprise-connection'];
$msg .= "\nMore Agents: ".$_POST['more-agents'];
$msg .= "\nMore Environments: ".$_POST['more-environments'];
$msg .= "\nHave Cluster Agents: ".$_POST['cluster-agents'];
$msg .= "\nHave All API Add-ons: ".$_POST['all-api-addons'];

$msg .= "\nEstimate: ".$_POST['price-range'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.mailboxvalidator.com/v1/validation/single?key=RYA34J7WT7GV5G2LO7RM&format=json&email=".$_POST['email']);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$validateEmailResults = curl_exec($ch);

if($results === FALSE) {
  die(curl_error($ch));
}

$validateEmailResults = json_decode($validateEmailResults);

$headers = "From: support@emergetech.com" . "\r\n";

if($validateEmailResults->is_verified == "True" && $validateEmailResults->is_disposable == "False" && $validateEmailResults->is_high_risk == "False") {

  // send email
  //mail("jeremy.mccourt@emergetech.com","Jitterbit Pricing Calculator - Contact Information", $msg, $headers);
  //mail("abigail.limpioso@emergetech.com", "Jitterbit Pricing Calculator - Contact Information", $msg, $headers);
  //mail("kaneisha.whipple@emergetech.com", "Jitterbit Pricing Calculator - Contact Information", $msg, $headers);

}

if($validateEmailResults->credits_available < 50) {
  $msg = 'You have less than 50 credits available for email validation API.';
  //mail("jeremy.mccourt@emergetech.com","Jitterbit Pricing Calculator - Email API Credits About to Run Out", $msg, $headers);
  //mail("abigail.limpioso@emergetech.com","Jitterbit Pricing Calculator - Email API Credits About to Run Out", $msg, $headers);
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Jitterbit Pricing Calculator</title>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="author" content="Abigail Limpioso">
    <meta name="description" content="Use the Jitterbit Pricing Calculator to determine what pricing plan is best for your company!">
    <meta property="og:title" content="Jitterbit Pricing Calculator" />
    <!-- STYLES -->
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="canonical" href="https://emergetech.com/jitterbit-pricing-calculator/" />

        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57965320-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());
     gtag('config', 'UA-57965320-1');
    </script>
</head>
  <body >
<header class="main-header">

  <div class='top-header'>
    <div class="logo">
        <a href="https://emergetech.com"><img src="https://emergetech.com/wp-content/uploads/2019/06/eMerge_Logo_LightBlue.png" alt="logo" id="logo"></a>
    </div>
  </div>

    </header>


<div class="main-content">

<!-- Jitterbit Quote -->

<div id="jitterbit-quote">

<p>The <span id="selected-plan"></span> is a good fit for you.</p>

<p>We estimate your price will be within the price range shown below:</p>
<p id="price-range">$<span id='count1'></span>k - $<span id='count2'></span>k per year</p>

<input type='button' onclick="backToQuoteForm()" id="back-btn" value="Back to Pricing Calculator"/>

</div>

<!-- SHOPIFY SALES LEAD FORM -->

<div class="sales-lead-section">

    <h3>Jitterbit is currently running a once in a lifetime sale, talk to an eMerge Technologies representative today to find out how we can save you up to 40% off list price!</h3>

    <form id="sales-lead-form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">

           <input type=hidden name='captcha_settings' value='{"keyname":"eMergeTech","fallback":"true","orgId":"00D41000000Tn5s","ts":""}'>
           <input type=hidden name="oid" value="00D41000000Tn5s">
           <input type=hidden name="retURL" value="https://emergetech.com/thank-you-shopify/">
           <input name="Shopify_Sales_Lead__c" type="hidden" value="yes"
           <input name="Campaign_ID" type="hidden" value="7012M000001nPUd" /><input id="lead_source" name="lead_source" type="hidden" value="website" />


           <!--  NOTE: These fields are optional debugging elements. Please uncomment    -->
           <!--  these lines if you wish to test in debug mode.                          -->
           <!--  <input type="hidden" name="debug" value=1>                              -->
           <!--  <input type="hidden" name="debugEmail"                                  -->
           <!--  value="jeremy.mccourt@emergetech.com">                                  -->

           <label for="first_name">First Name</label><input  id="first_name" maxlength="40" name="first_name" size="10" type="text" required="true" /><br>

           <label for="last_name">Last Name</label><input  id="last_name" maxlength="80" name="last_name" size="10" type="text" required="true" /><br>

           <label for="email">Email</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  id="email" maxlength="80" name="email" size="10" type="text" required="true" /><br>

           <label for="phone">Phone</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  id="phone" maxlength="40" name="phone" size="10" type="text" /><br>

           <label for="company">Company</label><input  id="company" maxlength="40" name="company" size="10" type="text" /><br>

           <label for="description">Message</label><br/><textarea id="message-field" name="description"></textarea><br>

           <input type="submit" id="connect-with-expert"  name="submit" value="Connect with a Jitterbit Expert">
           <br/>
           <label for="00N2M00000fFU8y">Uncheck this box to be removed from the eMergeTalks News Feed:</label><input id="00N2M00000fFU8y" checked="checked" name="00N2M00000fFU8y" type="checkbox" value="1" />
           </form>


</div>

    <footer>
        <hr>
        <p>&COPY; 2018 eMergeTech - All rights reserved. Terms and Conditions</p>
            <a href="https://www.facebook.com/eMergeTalk"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="https://plus.google.com/+Emergetech"><i class="fa fa-google-plus" aria-hidden="true"></i>
    </a>
            <a href="https://www.linkedin.com/company/emerge-technologies-inc-"><i class="fa fa-linkedin" aria-hidden="true"></i>
    </a>
            <a href="https://twitter.com/emergetalk"><i class="fa fa-twitter" aria-hidden="true"></i>
    </a>
    </footer>

    </div>

    <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script.js"></script>


</body>
</html>
