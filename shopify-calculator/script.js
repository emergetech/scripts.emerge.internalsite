

var aVg = document.getElementById("average");
var aVgOrder = 0;
var aVgMonthlySale = 0;
var aVgOnlineSale = 0;

var store = document.getElementById("instore");

var onl = document.getElementById("online");

var extent = document.getElementById("external");
var ext = document.getElementById("extNum")

aVg.oninput = function() {
  aVgOrder = this.value;
  aVg.innerHTML = this.value;
  //document.getElementById("basic").innerHTML=  calculate();
   calculate();
}


store.oninput = function() {
  aVgMonthlySale = this.value;
  store.innerHTML = this.value;
  //document.getElementById("basic").innerHTML=  calculate();
  calculate();
}
onl.oninput = function() {
  aVgOnlineSale = this.value;
  onl.innerHTML = this.value;
  //document.getElementById("basic").innerHTML=  calculate();
  calculate();

}

extent.oninput = function(){
  extent.innerHTML = this.value;
  ext = this.value;
  calculate();
}


document.getElementById("shopify-link").addEventListener("click", function(){

  var tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
  }

  document.getElementById("Shopify").style.display = "block";

  var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";

});

document.getElementById("external-link").addEventListener("click", function(){

    var tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    document.getElementById("External").style.display = "block";

    var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";

});

function calculate(){
  var monthlysale = parseInt(store.value)+parseInt(onl.value);
  var monthlyTrans = parseInt(monthlysale)*parseInt(aVg.value);
  var basic = 29;
  var ccPercBasic = 0.029;
  var ccFeeBasic = 0.30;
  var inPersonPercBasic = 0.27;

  var shopify = 79;
  var ccPercReg = 0.026;
  var ccFeeReg = 0.30
  var inPersonPercReg = 0.25;


  var advance = 299;
  var ccPercAdv = 0.024;
  var ccFeeAdv = 0.30;
  var inPersonPercAdv = 0.24;


  var plus = 2000;
  var retail = 0;
  var discount = 0;
  var period = 1;

  /****************************************
    Basic:
    Online - 2.9% + 0.30
    In Person - 2.7%

    Shopify:
    Online - 2.6% + 0.30
    In Person - 2.5%

    Advanced:
    Online - 2.4% + 0.30
    In Person - 2.4%
  ****************************************/
  var basictotal = parseFloat(((((aVg.value * ccPercBasic) + ccFeeBasic) * onl.value) + ((aVg.value * inPersonPercBasic) * store.value) + (basic + retail) - ((basic + retail) * discount)) * period).toFixed(2);

  var total =  parseFloat(((((aVg.value * ccPercReg) + ccFeeReg) * onl.value) + ((aVg.value * inPersonPercReg) * store.value) + (shopify + retail) - ((shopify + retail) * discount)) * period).toFixed(2);

  var advancetotal =  parseFloat(((((aVg.value * ccPercAdv) + ccFeeAdv) * onl.value) + ((aVg.value * inPersonPercAdv) * store.value) + (advance + retail) - ((advance + retail) * discount)) * period).toFixed(2);

  if(monthlyTrans >= 800000){
    var plus = monthlyTrans *0.0025;
     document.getElementById("plus").innerHTML = parseFloat(plus).toFixed(2);
  }else{
    var regPlus = ((((aVg.value * 0.0215) + 0.3) * onl.value) + ((aVg.value * 0.02) * store.value) + (plus + retail) - ((plus + retail) * discount)) * period;
    document.getElementById("plus").innerHTML = parseFloat(regPlus).toFixed(2);
  }

  document.getElementById("basic").innerHTML=  basictotal;
  document.getElementById("premium").innerHTML=  total;
  document.getElementById("advance").innerHTML=  advancetotal;

  /****************************************
    External Payment Gateway

    Basic:
    2.0%

    Shopify:
    1.0%

    Advanced:
    0.5%

    Plus:
    0.15%
  ****************************************/

  var externalPayBasic = 0.02;
  var externalPayReg = 0.01;
  var externalPayAdv = 0.005;
  var externalPayPlus = 0.0015;

  var eXtbasic = (extent.value * externalPayBasic) + basic;
  var eXtpremium = (extent.value * externalPayReg) + shopify;
  var eXtadvance = extent.value * externalPayAdv + advance;

    var eXtplus = (extent.value * externalPayPlus) + plus;

  document.getElementById("extBasic").innerHTML = parseFloat(eXtbasic).toFixed(2);
  document.getElementById("extPremium").innerHTML = parseFloat(eXtpremium).toFixed(2);
  document.getElementById("extAdvance").innerHTML = parseFloat(eXtadvance).toFixed(2);
  document.getElementById("extPlus").innerHTML = parseFloat(eXtplus).toFixed(2);

  return total;
}
